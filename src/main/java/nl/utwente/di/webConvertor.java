package nl.utwente.di;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.*;
import javax.servlet.http.*;


public class webConvertor extends HttpServlet{

    private static final long serialVersionUID = 1L;
    private Convertor convertor;

    public void init() throws ServletException {
        convertor = new Convertor();
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String docType =
                "<!DOCTYPE HTML>\n";
        String title = "Temperature Web Convertor";
        out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>Celsius temperature: " +
                request.getParameter("Celsius") + "\n" +
                "  <P>Fahrenheit temperature: " +
                Double.toString(convertor.Convert(request.getParameter("Celsius"))) +
                "</BODY></HTML>");
    }
}

