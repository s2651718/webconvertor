package nl.utwente.di;

public class Convertor {
    public double Convert(String celsium_in){
        if(celsium_in == null){
            return 5.0;
        }
        double celsium = Double.parseDouble(celsium_in);

        return (celsium*1.8)+32;
    }
}
